﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

	public Text scoreText;
	public Text hiScoreText;

	public float scoreCount;
	public float hiScoreCount;

	public float pointsPerSecond;

	public bool scoreIncreasing;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (scoreIncreasing) {
			scoreCount += pointsPerSecond * Time.deltaTime;
		}
		if (scoreCount > hiScoreCount) {
		
			hiScoreCount = scoreCount;
		}

		scoreText.text = "SCORE: " + Mathf.Round (scoreCount);
		hiScoreText.text = "HIGHSCORE: " +  Mathf.Round (hiScoreCount);

	}
}

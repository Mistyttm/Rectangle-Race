﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset : MonoBehaviour {
	public GameObject player;
	public Vector2 playerStartPoint;           
	private Rigidbody2D rb2d;       
	private ScoreManager theScoreManager;

	void Start()
	{
		
		rb2d = GetComponent<Rigidbody2D> ();

		theScoreManager = FindObjectOfType<ScoreManager>(); 
	}


	void FixedUpdate(){



	}

	//RESET SCRIPT
	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.CompareTag ("Respawn")) {
			//yield return new WaitForSeconds (0.5f);
			StartCoroutine (Example ());
			//Application.LoadLevel("MainMenu");
		}

		else if (other.gameObject.CompareTag ("Hat")) {
			Application.LoadLevel ("RectangleHat");
		}

	}




	IEnumerator Example()
	{

		yield return new WaitForSecondsRealtime(0.0f);
		transform.position = new Vector2(0.0f, 2.0f);
		theScoreManager.scoreCount = 0;
		theScoreManager.scoreIncreasing = true;
	}

}


